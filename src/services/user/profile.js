import axios from 'axios'

const API_HOST = process.env.VUE_APP_API_HOST

require('../../security/interceptors')

const profile = () => {
    return axios.get(`${API_HOST}/api/v1/user/profile`)
        .then(response => {
            return response
        })
        .catch(error => {
            return error.response
        })
}

const updateTour = (data) => {
    return axios.post(`${API_HOST}/api/v1/user/update-tour`, data)
        .then(response => {
            return response
        })
        .catch(error => {
            return error.response
        })
}

export default {
    profile,
    updateTour
}
