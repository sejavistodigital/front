import axios from 'axios'

const API_HOST = process.env.VUE_APP_API_HOST

require('../../security/interceptors')

const list = (page, perPage) => {
    return axios.get(`${API_HOST}/api/v1/clients/list/${page}/${perPage}`)
        .then(response => {
            return response
        })
        .catch(error => {
            return error.response
        })
}

export default {
    list
}
