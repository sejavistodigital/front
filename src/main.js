import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

// LODASH
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

// MASK INPUT
import VueMask from 'v-mask'

// VUE TOUR
import VueTour from 'vue-tour'
require('vue-tour/dist/vue-tour.css')

// CHARTS
import VueApexCharts from 'vue-apexcharts'

// VUESAX UI
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'

// Import and mounted components customns
import allComponents from './components'

// CREATED DATABASE
import { initTable } from './utils/db'
initTable()

Vue.config.productionTip = false

Vue.use(VueLodash, { lodash: lodash })
Vue.use(VueMask)
Vue.use(VueTour)
Vue.use(VueApexCharts)
Vue.use(Vuesax)
Vue.component('apexchart', VueApexCharts)

for (var item of allComponents) {
  Vue.component(item.name, item.component)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
