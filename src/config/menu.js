const menu = [
    {
        name: 'Dashboard',
        icon: 'fas fa-tachometer-alt',
        route: 'painel.dashboard',
        rules: ['A', 'U', 'S']
    },
    {
        name: 'Cadastros',
        icon: 'fas fa-plus-circle',
        rules: ['A', 'U', 'S'],
        submenu: [
            {
                name: 'Entidades',
                icon: 'fas fa-users',
                route: 'painel.register.entities',
                rules: ['A', 'U', 'S']
            },
            {
                name: 'Produtos',
                icon: 'fas fa-cubes',
                route: 'painel.register.products',
                rules: ['A', 'U', 'S']
            }
        ]
    },
    {
        name: 'Financeiro',
        icon: 'fas fa-dollar-sign',
        route: 'painel.financial',
        rules: ['A', 'U', 'S']
    },
    {
        name: 'Configurações',
        icon: 'fas fa-cogs',
        route: 'painel.config',
        rules: ['A', 'U', 'S']
    }
]

export default {
    menu
}
