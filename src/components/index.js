import Header from './Header'

/**
 * Aqui você deve registrar todos os componentes personalizados
 * que foram criados na pasta '/components'
 */
export default [
  { name: 'sv-header', component: Header }
]
