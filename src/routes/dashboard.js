export default [
  {
    path: '/painel',
    component: () => import(/* webpackChunkName: "autenticated" */ './../layouts/Authenticated.vue'),
    meta: { auth: false },
    children: [
      {
        path: 'dashboard',
        name: 'painel.dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ './../pages/painel/Dashboard.vue'),
        meta: { auth: false }
      },
      {
        path: 'register/entities',
        name: 'painel.register.entities',
        component: () => import(/* webpackChunkName: "entities" */ './../pages/painel/Entities.vue'),
        meta: { auth: false }
      },
      {
        path: 'register/products',
        name: 'painel.register.products',
        component: () => import(/* webpackChunkName: "products" */ './../pages/painel/Products.vue'),
        meta: { auth: false }
      },
      {
        path: 'financial',
        name: 'painel.financial',
        component: () => import(/* webpackChunkName: "financial" */ './../pages/painel/Financial.vue'),
        meta: { auth: false }
      },
      {
        path: 'config',
        name: 'painel.config',
        component: () => import(/* webpackChunkName: "config" */ './../pages/painel/Config.vue'),
        meta: { auth: false }
      }
    ]
  }
]
