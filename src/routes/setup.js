export default [{
    path: '/setup',
    component: () =>
        import ( /* webpackChunkName: "template-setup" */ '../layouts/Setup.vue'),
    meta: { auth: false },
    children: [{
            path: '',
            name: 'setup.welcome',
            component: () =>
                import ( /* webpackChunkName: "welcome" */ '../pages/setup/Welcome.vue'),
            meta: {
                auth: false
            }
        },
        {
            path: 'connection',
            name: 'setup.connection',
            component: () =>
                import ( /* webpackChunkName: "connection" */ '../pages/setup/Connection.vue'),
            meta: {
                auth: false
            }
        },
        {
            path: 'company',
            name: 'setup.company',
            component: () =>
                import ( /* webpackChunkName: "company" */ '../pages/setup/Company.vue'),
            meta: {
                auth: false
            }
        },
        {
            path: 'user',
            name: 'setup.user',
            component: () =>
                import ( /* webpackChunkName: "user" */ '../pages/setup/User.vue'),
            meta: {
                auth: false
            }
        }
    ]
}]