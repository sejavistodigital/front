export default [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "template-unauthenticated" */ './../layouts/Unauthenticated.vue'),
    meta: { auth: false },
    children: [
      {
        path: '',
        name: 'auth.login',
        component: () => import(/* webpackChunkName: "login" */ './../pages/auth/Login.vue'),
        meta: {
          auth: false
        }
      },
      {
        path: 'reset-password',
        name: 'auth.reset-password',
        component: () => import(/* webpackChunkName: "reset-password" */ './../pages/auth/ResetPassword.vue'),
        meta: {
          auth: false
        }
      },
      {
        path: 'update-password/:token/:username',
        name: 'auth.update-password',
        component: () => import(/* webpackChunkName: "update-password" */ './../pages/auth/UpdatePassword.vue'),
        meta: {
          auth: false
        }
      }
    ]
  }
]
