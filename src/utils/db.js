var sqlite3 = require('sqlite3').verbose();
let db

function conn () {
    if (!db || !db.open) {
        db = new sqlite3.Database('config.db')
    }

    return db
}

export const initTable = () => {
    return new Promise((resolve) => {
        let db = conn()
        db.serialize(() => {
            db.run('CREATE TABLE if not exists CheckSetup (id int primary key, finished varchar(64))')
            db.run('CREATE TABLE if not exists ConnectionApi (id int primary key, url varchar(255))')
            resolve()
        })
    })
}

export const checkSetup = () => {
    return new Promise((resolve, reject) =>  {
        let db = conn()
        db.all('SELECT finished FROM CheckSetup', (err, rows) => {
            if (err) reject(err)
            resolve(rows || [])
        })
    })
}

export const updateSetup = (status) => {
    return new Promise((resolve) => {
        let db = conn()
        let data = db.prepare('REPLACE INTO CheckSetup (finished) values (?)')
        data.run(status)
        data.finalize(err => {
            if (!err) resolve()
        })
    })
}

// SAVE API URL
export const saveApiUrl = (apiUrl) => {
    return new Promise((resolve) => {
        let db = conn()
        let data = db.prepare('REPLACE INTO ConnectionApi (url) values (?)')
        data.run(apiUrl)
        data.finalize(err => {
            if (!err) resolve()
        })
    })
}

export const getApiUrl = () => {
    return new Promise((resolve, reject) =>  {
        let db = conn()
        db.all('SELECT url FROM ConnectionApi', (err, rows) => {
            if (err) reject(err)
            resolve(rows || [])
        })
    })
}